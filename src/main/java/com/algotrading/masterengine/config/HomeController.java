package com.algotrading.masterengine.config;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @RequestMapping(value = "/home")
    public String index() {
        System.out.println("swagger-ui.html");

        return "redirect:/swagger-ui.html";
    }
}

package com.algotrading.masterengine.controller;

import com.algotrading.masterengine.model.SymbolAnalisys;
import com.algotrading.masterengine.service.InvestDecisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping({"/investDecisor"})
public class InvestDecisorController {

    @Autowired
    public InvestDecisorService investDecisor;

    @RequestMapping(value = "/bySymbol/{symbol}", method = RequestMethod.GET)
    public SymbolAnalisys analyzeSymbol(@PathVariable(value="symbol") String symbol) {
        return investDecisor.analyze(symbol);
    }

    @RequestMapping(value = "shouldSell/{user}/{symbol}", method = RequestMethod.GET)
    public boolean shouldSell(@PathVariable(value="user") String userId,
                              @PathVariable(value="symbol") String symbol) {
        return investDecisor.shoudlSell(symbol, userId);
    }

    @RequestMapping(value = "shouldBuy/{user}/{symbol}", method = RequestMethod.GET)
    public boolean shouldBuy(@PathVariable(value="user") String userId,
                              @PathVariable(value="symbol") String symbol) {
        return investDecisor.shouldBuy(symbol, userId);
    }
}

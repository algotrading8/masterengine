package com.algotrading.masterengine.controller;

import com.algotrading.masterengine.model.CryptoCompareFullSymbol;
import com.algotrading.masterengine.model.WalletDetail;
import com.algotrading.masterengine.model.WalletTransaction;
import com.algotrading.masterengine.model.WalletValue;
import com.algotrading.masterengine.service.WalletValueService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping({"/userwallet"})
public class WalletValueController {

    @Autowired
    private WalletValueService walletValueService;

    @RequestMapping(value = "/value", method = RequestMethod.POST)
    public List<WalletValue> valueUserWallet(@RequestBody String userId) {
        return walletValueService.valueUserWallet(userId);
    }

    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    public List<WalletTransaction> transactionUserWallet(@RequestBody String userId) {
        return walletValueService.transactionUserWallet(userId);
    }

    @RequestMapping(value = "/chartValue", method = RequestMethod.POST)
    public List<Double> chartValueUserWallet(@RequestBody List<WalletDetail> walletDetails) {
        return walletValueService.chartValueUserWallet(walletDetails);
    }

    @RequestMapping(value = "/chartLabel", method = RequestMethod.GET)
    public List<String> chartLabelUserWallet() {
        return walletValueService.chartLabelUserWallet();
    }

    @RequestMapping(value = "/insertWalletDetail" , method = RequestMethod.POST)
    public int insertWalletDetail(@RequestBody WalletDetail walletDetail) throws JsonProcessingException {
        return walletValueService.insertWalletDetail(walletDetail);
    }

    @RequestMapping(value = "/insertWalletTransaction" , method = RequestMethod.POST)
    public int insertWalletTransaction(@RequestBody WalletTransaction walletTransaction) throws JsonProcessingException {
        return walletValueService.insertWalletTransaction(walletTransaction);
    }

    @RequestMapping(value = "/getFullSymbol/{symbol}/{ccy}" , method = RequestMethod.GET)
    public CryptoCompareFullSymbol getFullSymbol(@PathVariable(value="symbol") String symbol,
                                                 @PathVariable(value="ccy") String ccy) throws JsonProcessingException {
        return walletValueService.getFullSymbol(symbol, ccy);
    }
}

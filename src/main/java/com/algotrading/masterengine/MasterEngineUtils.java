package com.algotrading.masterengine;

import com.algotrading.masterengine.model.CryptoCompareFullSymbol;
import com.algotrading.masterengine.model.CryptoComparePrice;
import com.algotrading.masterengine.model.WalletDetail;
import com.algotrading.masterengine.model.WalletTransaction;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MasterEngineUtils {

    public static List<WalletDetail> getWalletDetail(String result) {
        List<WalletDetail> walletDetails = new ArrayList<>();
        Gson gson = new Gson();

        try{
            JSONArray jsonArray = new JSONArray(result);

            for (int i = 0, size = jsonArray.length(); i < size; i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                WalletDetail priceObject = gson.fromJson(json.toString(), WalletDetail.class);
                walletDetails.add(priceObject);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return walletDetails;
    }

    public static Double getPrice(String result) {
        Double price = 0.0;
        Gson gson = new Gson();

        try{
            JSONObject jsonObject = new JSONObject(result);
            CryptoComparePrice priceObject = gson.fromJson(jsonObject.toString(), CryptoComparePrice.class);
            price = priceObject.getPrice();

        } catch (Exception e){
            e.printStackTrace();
        }

        return price;
    }

    public static List<WalletTransaction> getWalletTransaction(String result) {
        List<WalletTransaction> walletTransactions = new ArrayList<>();
        Gson gson = new Gson();

        try{
            JSONArray jsonArray = new JSONArray(result);

            for (int i = 0, size = jsonArray.length(); i < size; i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                WalletTransaction transactionObject = gson.fromJson(json.toString(), WalletTransaction.class);
                walletTransactions.add(transactionObject);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return walletTransactions;
    }

    public static List<Double> getListPrice(String body, Double qty) {
        List<Double> prices = new ArrayList<>();
        Gson gson = new Gson();

        try{
            JSONArray jsonArray = new JSONArray(body);

            for (int i = 0, size = jsonArray.length(); i < size; i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                prices.add(json.getDouble("price") * qty);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return prices;
    }

    public static List<CryptoComparePrice> getCriptoPrice(String result) {
        List<CryptoComparePrice> priceList = new ArrayList<>();
        Gson gson = new Gson();

        try{
            JSONArray jsonArray = new JSONArray(result);

            for (int i = 0, size = jsonArray.length(); i < size; i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                CryptoComparePrice priceObject = gson.fromJson(json.toString(), CryptoComparePrice.class);
                priceList.add(priceObject);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return priceList;
    }

    public static CryptoCompareFullSymbol getFullSymbol(String body) {
        Gson gson = new Gson();
        CryptoCompareFullSymbol priceObject = new CryptoCompareFullSymbol();

        try{
            JSONObject jsonObject = new JSONObject(body);
            priceObject = gson.fromJson(jsonObject.toString(), CryptoCompareFullSymbol.class);
        } catch (Exception e){
            e.printStackTrace();
        }

        return priceObject;
    }
}

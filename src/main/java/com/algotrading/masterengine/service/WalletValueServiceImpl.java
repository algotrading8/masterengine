package com.algotrading.masterengine.service;

import com.algotrading.masterengine.MasterEngineUtils;
import com.algotrading.masterengine.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.algotrading.masterengine.MasterEngineConstant.*;

@Service
public class WalletValueServiceImpl implements WalletValueService{
    @Override
    public List<WalletValue> valueUserWallet(String userId) {
        List<WalletValue> walletValues = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        UriComponentsBuilder builderWallet = UriComponentsBuilder.fromHttpUrl(walletEngineURL +
                walletDetailById +
                userId);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<String> response = restTemplate.exchange(
                builderWallet.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        List<WalletDetail> walletDetails = MasterEngineUtils.getWalletDetail(response.getBody());

        for (WalletDetail walletDetail: walletDetails) {
            UriComponentsBuilder builderPrice = UriComponentsBuilder.fromHttpUrl(priceEngineURL +
                    priceBySymbol.replace(":symbol",  walletDetail.getSymbol()));
            HttpEntity<String> responsePrice = restTemplate.exchange(
                    builderPrice.toUriString(),
                    HttpMethod.GET,
                    entity,
                    String.class);
            Double price = MasterEngineUtils.getPrice(responsePrice.getBody());

            WalletValue walletRecord = new WalletValue();
            walletRecord.setBuyPrice(walletDetail.getBuyPrice());
            walletRecord.setQty(walletDetail.getQty());
            walletRecord.setSymbol(walletDetail.getSymbol());
            walletRecord.setActualPrice(price);
            walletRecord.setTotalActualPrice(price * walletRecord.getQty());
            walletRecord.setTotalBuyPrice(walletDetail.getBuyPrice() * walletRecord.getQty());
            Double pl = getPl(price, walletRecord.getBuyPrice());
            walletRecord.setPl(pl);

            walletValues.add(walletRecord);
        }

        return walletValues;
    }

    @Override
    public List<WalletTransaction> transactionUserWallet(String userId) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        UriComponentsBuilder builderWallet = UriComponentsBuilder.fromHttpUrl(walletEngineURL +
                fullWalletTransaction +
                userId);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<String> response = restTemplate.exchange(
                builderWallet.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        return MasterEngineUtils.getWalletTransaction(response.getBody());
    }

    @Override
    public List<Double> chartValueUserWallet(List<WalletDetail> walletDetails) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        List<Double> pricing = new ArrayList<>(Collections.nCopies(24, 0.0));

        for (WalletDetail walletDetail: walletDetails) {
            UriComponentsBuilder builderPrice = UriComponentsBuilder.fromHttpUrl(priceEngineURL +
                    ohlcPriceBySymbol.replace(":symbol",  walletDetail.getSymbol()));

            HttpEntity<String> responsePrice = restTemplate.exchange(
                    builderPrice.toUriString(),
                    HttpMethod.GET,
                    entity,
                    String.class);

            List<Double> price = MasterEngineUtils.getListPrice(responsePrice.getBody(), walletDetail.getQty());
            for (int i = 0; i < price.size(); i++) {
                pricing.set(i, Math.floor((pricing.get(i) + price.get(i)) * 100) / 100);
            }
        }

        return pricing;
    }

    @Override
    public List<String> chartLabelUserWallet() {
        List<String> labels = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        for (int i = 24; i > 0; i--) {
            String dateTime = LocalDateTime.now().minusHours(i).format(formatter);
            labels.add(dateTime);
        }

        return labels;
    }

    @Override
    public int insertWalletDetail(WalletDetail walletDetail) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        String url = walletEngineURL + insertWalletDetail;

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(walletDetail);
        HttpEntity<WalletDetail> entity = new HttpEntity<WalletDetail>(walletDetail,headers);
        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

        return 1;

    }

    @Override
    public int insertWalletTransaction(WalletTransaction walletTransaction) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        String url = walletEngineURL + insertWalletTransaction;

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(walletTransaction);
        HttpEntity<WalletTransaction> entity = new HttpEntity<WalletTransaction>(walletTransaction,headers);
        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

        return 1;

    }

    @Override
    public CryptoCompareFullSymbol getFullSymbol(String symbol, String ccy) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String url = priceEngineURL + fullSymbolInfo;
        url = url.replace(":symbol", symbol);
        url = url.replace(":ccy", ccy);
        UriComponentsBuilder builderWallet = UriComponentsBuilder.fromHttpUrl(url);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<String> response = restTemplate.exchange(
                builderWallet.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        return MasterEngineUtils.getFullSymbol(response.getBody());
    }

    private double getPl(Double price, Double buyPrice) {
        double pl = (price / buyPrice) * 100;
        if(price < buyPrice) {
            pl = pl - 100;
        }
        return pl;
    }

    public List<WalletDetail> obtainWalletBySymbol(String userId, String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        String url = walletEngineURL + symbolWalletDetail;
        url = url.replace(":symbol", symbol);
        url = url.replace(":userid", userId);
        UriComponentsBuilder builderWallet = UriComponentsBuilder.fromHttpUrl(url);

        HttpEntity<String> response = restTemplate.exchange(
                builderWallet.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        return MasterEngineUtils.getWalletDetail(response.getBody());
    }

    public List<CryptoComparePrice> getOHCLBySymbol(String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        UriComponentsBuilder builderPrice = UriComponentsBuilder.fromHttpUrl(priceEngineURL +
                ohlcPriceBySymbolMin.replace(":symbol",  symbol));

        HttpEntity<String> responsePrice = restTemplate.exchange(
                builderPrice.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        return MasterEngineUtils.getCriptoPrice(responsePrice.getBody());
    }
}

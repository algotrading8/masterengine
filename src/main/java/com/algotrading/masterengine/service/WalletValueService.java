package com.algotrading.masterengine.service;

import com.algotrading.masterengine.model.CryptoCompareFullSymbol;
import com.algotrading.masterengine.model.WalletDetail;
import com.algotrading.masterengine.model.WalletTransaction;
import com.algotrading.masterengine.model.WalletValue;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

public interface WalletValueService {

    List<WalletValue> valueUserWallet(String userId);

    List<WalletTransaction> transactionUserWallet(String userId);

    List<Double> chartValueUserWallet(List<WalletDetail> walletDetails);

    List<String> chartLabelUserWallet();

    int insertWalletDetail(WalletDetail walletDetail) throws JsonProcessingException;

    int insertWalletTransaction(WalletTransaction walletTransaction) throws JsonProcessingException;

    CryptoCompareFullSymbol getFullSymbol(String symbol, String ccy);
}

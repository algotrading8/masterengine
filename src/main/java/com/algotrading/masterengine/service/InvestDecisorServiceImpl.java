package com.algotrading.masterengine.service;

import com.algotrading.masterengine.model.CryptoCompareFullSymbol;
import com.algotrading.masterengine.model.CryptoComparePrice;
import com.algotrading.masterengine.model.SymbolAnalisys;
import com.algotrading.masterengine.model.WalletDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.algotrading.masterengine.MasterEngineConstant.*;

@Service
public class InvestDecisorServiceImpl implements InvestDecisorService{

    @Autowired
    public WalletValueServiceImpl walletValueService;

    /*
    1. Deci
    */

    @Override
    public SymbolAnalisys analyze(String symbol) {
        return null;
    }

    /* Venderemos si:
        1. El precio actual es inferior al de compra y predecimos descenso
            + no creemos que se alcance ese precio de nuevo.
        2. El precio actual es superior al de compra y predecimos descenso.

        Si se realiza la transacción introduciremos un registro en la WalletTransaction
    */
    @Override
    public boolean shoudlSell(String symbol, String userId) {
        List<WalletDetail> walletDetails = walletValueService.obtainWalletBySymbol(userId, symbol);
        List<CryptoComparePrice> prices = walletValueService.getOHCLBySymbol(symbol);
        boolean result = false;
        int lastIndex = prices.size();
        String trend = obtainTrendFromPrice(prices, symbol, "EUR");

        for (WalletDetail walletDetail : walletDetails) {
            if(trend.equals(downTrend)) {
                result = true;
                if(walletDetail.getBuyPrice() * marginSpread > prices.get(lastIndex).getPrice()){
                    result = false;
                }
            }
        }

        return result;
    }

    private String obtainTrendFromPrice(List<CryptoComparePrice> prices, String symbol, String ccy) {
        String result = downTrend;
        CryptoCompareFullSymbol fullSymbolInfo = walletValueService.getFullSymbol(symbol, ccy);


        return result;
    }

    /* Compraremos si:
        1. Predecimos ascenso.

        Si se realiza la transacción introduciremos un registro en la WalletTransaction
    */
    @Override
    public boolean shouldBuy(String symbol, String userId) {
        List<WalletDetail> walletDetails = walletValueService.obtainWalletBySymbol(userId, symbol);
        List<CryptoComparePrice> prices = walletValueService.getOHCLBySymbol(symbol);
        boolean result = false;
        String trend = obtainTrendFromPrice(prices, symbol, "EUR");

        for (WalletDetail walletDetail : walletDetails) {
            if(trend.equals(upTrend)) {
                result = true;
            }
        }

        return result;
    }
}

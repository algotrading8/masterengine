package com.algotrading.masterengine.service;

import com.algotrading.masterengine.model.SymbolAnalisys;

public interface InvestDecisorService {
    SymbolAnalisys analyze(String symbol);

    boolean shoudlSell(String symbol, String userId);

    boolean shouldBuy(String symbol, String userId);
}

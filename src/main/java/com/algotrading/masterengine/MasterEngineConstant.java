package com.algotrading.masterengine;

public class MasterEngineConstant {
    public static String walletEngineURL = "http://localhost:8081/";
    public static String priceEngineURL = "http://localhost:8080/";
    public static String walletDetailById = "users/getFullWalletById/";
    public static String fullWalletTransaction = "users/getFullWalletTransaction/";
    public static String priceBySymbol = "cryptocompare/priceSymbolCcy/:symbol/EUR";
    public static String ohlcPriceBySymbol = "cryptocompare/OCHLSymbolCcy/:symbol/EUR/hour/23";
    public static String ohlcPriceBySymbolMin = "cryptocompare/OCHLSymbolCcy/:symbol/EUR/minute/60";
    public static String fullSymbolInfo = "cryptocompare/fullSymbolCcy/:symbol/:ccy";
    public static String insertWalletDetail = "users/insertWalletDetail";
    public static String insertWalletTransaction = "users/insertWalletTransaction";
    public static String symbolWalletDetail = "users/getSymbolWalletById/:symbol/:userid";
    public static String downTrend = "Down";
    public static String upTrend = "Up";
    public static Double marginSpread = 1.1;
}

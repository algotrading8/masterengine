package com.algotrading.masterengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasterEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasterEngineApplication.class, args);
	}

}

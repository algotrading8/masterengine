package com.algotrading.masterengine.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CryptoComparePrice {
    @SerializedName("symbol")
    private String symbol;
    @SerializedName("source")
    private String source;
    @SerializedName("currency")
    private String currency;
    @SerializedName("isValid")
    private boolean isValid;
    @SerializedName("timestamp")
    private Date timestamp;
    @SerializedName("price")
    private Double price;
    @SerializedName("volumeTo")
    private Double volumeTo;
    @SerializedName("volumeFrom")
    private Double volumeFrom;
    @SerializedName("open")
    private Double open;
    @SerializedName("close")
    private Double close;
    @SerializedName("high")
    private Double high;
    @SerializedName("low")
    private Double low;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getVolumeTo() {
        return volumeTo;
    }

    public void setVolumeTo(Double volumeTo) {
        this.volumeTo = volumeTo;
    }

    public Double getVolumeFrom() {
        return volumeFrom;
    }

    public void setVolumeFrom(Double volumeFrom) {
        this.volumeFrom = volumeFrom;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }
}

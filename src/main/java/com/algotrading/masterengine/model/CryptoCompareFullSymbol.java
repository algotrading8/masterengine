package com.algotrading.masterengine.model;

import com.google.gson.annotations.SerializedName;

public class CryptoCompareFullSymbol {
    @SerializedName("symbol")
    private String symbol;
    @SerializedName("currency")
    private String currency;
    @SerializedName("MARKET")
    private String market;
    @SerializedName("price")
    private Double price;
    @SerializedName("lastUpdate")
    private String lastUpdate;
    @SerializedName("lastVolume")
    private Double lastVolume;
    @SerializedName("median")
    private Double median;
    @SerializedName("lastVolumeTo")
    private Double lastVolumeTo;
    @SerializedName("lastTradeId")
    private Double lastTradeId;
    @SerializedName("volumeDay")
    private Double volumeDay;
    @SerializedName("volumeDayTo")
    private Double volumeDayTo;
    @SerializedName("volumeDay24h")
    private Double volumeDay24h;
    @SerializedName("volumeDay24hTo")
    private Double volumeDay24hTo;
    @SerializedName("openDay")
    private Double openDay;
    @SerializedName("highDay")
    private Double highDay;
    @SerializedName("lowDay")
    private Double lowDay;
    @SerializedName("open24h")
    private Double open24h;
    @SerializedName("high24h")
    private Double high24h;
    @SerializedName("low24h")
    private Double low24h;
    @SerializedName("lastMarket")
    private String lastMarket;
    @SerializedName("volumeHour")
    private Double volumeHour;
    @SerializedName("volumeHourTo")
    private Double volumeHourTo;
    @SerializedName("openHour")
    private Double openHour;
    @SerializedName("highHour")
    private Double highHour;
    @SerializedName("lowHour")
    private Double lowHour;
    @SerializedName("topTierVolume24Hour")
    private Double topTierVolume24Hour;
    @SerializedName("topTierVolume24HourTo")
    private Double topTierVolume24HourTo;
    @SerializedName("change24h")
    private Double change24h;
    @SerializedName("changePct24h")
    private Double changePct24h;
    @SerializedName("changeDay")
    private Double changeDay;
    @SerializedName("changePctDay")
    private Double changePctDay;
    @SerializedName("changeHour")
    private Double changeHour;
    @SerializedName("changePctHour")
    private Double changePctHour;
    @SerializedName("conversionType")
    private String conversionType;
    @SerializedName("conversionSymbol")
    private String conversionSymbol;
    @SerializedName("supply")
    private Double supply;
    @SerializedName("mktCap")
    private Double mktCap;
    @SerializedName("mktCapPenalty")
    private Double mktCapPenalty;
    @SerializedName("circulatingSupply")
    private Double circulatingSupply;
    @SerializedName("circulatingSupplyMktCap")
    private Double circulatingSupplyMktCap;
    @SerializedName("totalVolume24h")
    private Double totalVolume24h;
    @SerializedName("totalVolume24hTo")
    private Double totalVolume24hTo;
    @SerializedName("totalTopTierVolume24h")
    private Double totalTopTierVolume24h;
    @SerializedName("totalTopTierVolume24hTo")
    private Double totalTopTierVolume24hTo;
    @SerializedName("imageUrl")
    private String imageUrl;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Double getLastVolume() {
        return lastVolume;
    }

    public void setLastVolume(Double lastVolume) {
        this.lastVolume = lastVolume;
    }

    public Double getLastVolumeTo() {
        return lastVolumeTo;
    }

    public void setLastVolumeTo(Double lastVolumeTo) {
        this.lastVolumeTo = lastVolumeTo;
    }

    public Double getLastTradeId() {
        return lastTradeId;
    }

    public void setLastTradeId(Double lastTradeId) {
        this.lastTradeId = lastTradeId;
    }

    public Double getVolumeDay() {
        return volumeDay;
    }

    public void setVolumeDay(Double volumeDay) {
        this.volumeDay = volumeDay;
    }

    public Double getVolumeDayTo() {
        return volumeDayTo;
    }

    public void setVolumeDayTo(Double volumeDayTo) {
        this.volumeDayTo = volumeDayTo;
    }

    public Double getVolumeDay24h() {
        return volumeDay24h;
    }

    public void setVolumeDay24h(Double volumeDay24h) {
        this.volumeDay24h = volumeDay24h;
    }

    public Double getVolumeDay24hTo() {
        return volumeDay24hTo;
    }

    public void setVolumeDay24hTo(Double volumeDay24hTo) {
        this.volumeDay24hTo = volumeDay24hTo;
    }

    public Double getOpenDay() {
        return openDay;
    }

    public void setOpenDay(Double openDay) {
        this.openDay = openDay;
    }

    public Double getHighDay() {
        return highDay;
    }

    public void setHighDay(Double highDay) {
        this.highDay = highDay;
    }

    public Double getLowDay() {
        return lowDay;
    }

    public void setLowDay(Double lowDay) {
        this.lowDay = lowDay;
    }

    public Double getOpen24h() {
        return open24h;
    }

    public void setOpen24h(Double open24h) {
        this.open24h = open24h;
    }

    public Double getHigh24h() {
        return high24h;
    }

    public void setHigh24h(Double high24h) {
        this.high24h = high24h;
    }

    public Double getLow24h() {
        return low24h;
    }

    public void setLow24h(Double low24h) {
        this.low24h = low24h;
    }

    public String getLastMarket() {
        return lastMarket;
    }

    public void setLastMarket(String lastMarket) {
        this.lastMarket = lastMarket;
    }

    public Double getVolumeHour() {
        return volumeHour;
    }

    public void setVolumeHour(Double volumeHour) {
        this.volumeHour = volumeHour;
    }

    public Double getVolumeHourTo() {
        return volumeHourTo;
    }

    public void setVolumeHourTo(Double volumeHourTo) {
        this.volumeHourTo = volumeHourTo;
    }

    public Double getOpenHour() {
        return openHour;
    }

    public void setOpenHour(Double openHour) {
        this.openHour = openHour;
    }

    public Double getHighHour() {
        return highHour;
    }

    public void setHighHour(Double highHour) {
        this.highHour = highHour;
    }

    public Double getLowHour() {
        return lowHour;
    }

    public void setLowHour(Double lowHour) {
        this.lowHour = lowHour;
    }

    public Double getTopTierVolume24Hour() {
        return topTierVolume24Hour;
    }

    public void setTopTierVolume24Hour(Double topTierVolume24Hour) {
        this.topTierVolume24Hour = topTierVolume24Hour;
    }

    public Double getTopTierVolume24HourTo() {
        return topTierVolume24HourTo;
    }

    public void setTopTierVolume24HourTo(Double topTierVolume24HourTo) {
        this.topTierVolume24HourTo = topTierVolume24HourTo;
    }

    public Double getChange24h() {
        return change24h;
    }

    public void setChange24h(Double change24h) {
        this.change24h = change24h;
    }

    public Double getChangePct24h() {
        return changePct24h;
    }

    public void setChangePct24h(Double changePct24h) {
        this.changePct24h = changePct24h;
    }

    public Double getChangeDay() {
        return changeDay;
    }

    public void setChangeDay(Double changeDay) {
        this.changeDay = changeDay;
    }

    public Double getChangePctDay() {
        return changePctDay;
    }

    public void setChangePctDay(Double changePctDay) {
        this.changePctDay = changePctDay;
    }

    public Double getChangeHour() {
        return changeHour;
    }

    public void setChangeHour(Double changeHour) {
        this.changeHour = changeHour;
    }

    public Double getChangePctHour() {
        return changePctHour;
    }

    public void setChangePctHour(Double changePctHour) {
        this.changePctHour = changePctHour;
    }

    public String getConversionType() {
        return conversionType;
    }

    public void setConversionType(String conversionType) {
        this.conversionType = conversionType;
    }

    public String getConversionSymbol() {
        return conversionSymbol;
    }

    public void setConversionSymbol(String conversionSymbol) {
        this.conversionSymbol = conversionSymbol;
    }

    public Double getSupply() {
        return supply;
    }

    public void setSupply(Double supply) {
        this.supply = supply;
    }

    public Double getMktCap() {
        return mktCap;
    }

    public void setMktCap(Double mktCap) {
        this.mktCap = mktCap;
    }

    public Double getMktCapPenalty() {
        return mktCapPenalty;
    }

    public void setMktCapPenalty(Double mktCapPenalty) {
        this.mktCapPenalty = mktCapPenalty;
    }

    public Double getCirculatingSupply() {
        return circulatingSupply;
    }

    public void setCirculatingSupply(Double circulatingSupply) {
        this.circulatingSupply = circulatingSupply;
    }

    public Double getCirculatingSupplyMktCap() {
        return circulatingSupplyMktCap;
    }

    public void setCirculatingSupplyMktCap(Double circulatingSupplyMktCap) {
        this.circulatingSupplyMktCap = circulatingSupplyMktCap;
    }

    public Double getTotalVolume24h() {
        return totalVolume24h;
    }

    public void setTotalVolume24h(Double totalVolume24h) {
        this.totalVolume24h = totalVolume24h;
    }

    public Double getTotalVolume24hTo() {
        return totalVolume24hTo;
    }

    public void setTotalVolume24hTo(Double totalVolume24hTo) {
        this.totalVolume24hTo = totalVolume24hTo;
    }

    public Double getTotalTopTierVolume24h() {
        return totalTopTierVolume24h;
    }

    public void setTotalTopTierVolume24h(Double totalTopTierVolume24h) {
        this.totalTopTierVolume24h = totalTopTierVolume24h;
    }

    public Double getTotalTopTierVolume24hTo() {
        return totalTopTierVolume24hTo;
    }

    public void setTotalTopTierVolume24hTo(Double totalTopTierVolume24hTo) {
        this.totalTopTierVolume24hTo = totalTopTierVolume24hTo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getMedian() {
        return median;
    }

    public void setMedian(Double median) {
        this.median = median;
    }
}

package com.algotrading.masterengine.model;

public class SymbolAnalisys {
    private String symbol;
    private String currency;
    private Double investInd;
    private Double investScore;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getInvestInd() {
        return investInd;
    }

    public void setInvestInd(Double investInd) {
        this.investInd = investInd;
    }

    public Double getInvestScore() {
        return investScore;
    }

    public void setInvestScore(Double investScore) {
        this.investScore = investScore;
    }
}

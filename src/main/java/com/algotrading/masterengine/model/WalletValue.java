package com.algotrading.masterengine.model;

import org.springframework.stereotype.Component;

@Component
public class WalletValue {

    private String symbol;
    private Double qty;
    private Double totalActualPrice;
    private Double totalBuyPrice;
    private Double buyPrice;
    private Double actualPrice;
    private Double pl;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getTotalActualPrice() {
        return totalActualPrice;
    }

    public void setTotalActualPrice(Double totalActualPrice) {
        this.totalActualPrice = totalActualPrice;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Double getTotalBuyPrice() {
        return totalBuyPrice;
    }

    public void setTotalBuyPrice(Double totalBuyPrice) {
        this.totalBuyPrice = totalBuyPrice;
    }

    public Double getPl() {
        return pl;
    }

    public void setPl(Double pl) {
        this.pl = pl;
    }
}

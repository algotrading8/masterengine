package com.algotrading.masterengine.model;

import com.google.gson.annotations.SerializedName;
import org.springframework.stereotype.Component;

@Component
public class WalletDetail {

    @SerializedName("detailId")
    private Integer detailId;

    @SerializedName("walletId")
    private String walletId;

    @SerializedName("symbol")
    private String symbol;

    @SerializedName("originSource")
    private String originSource;

    @SerializedName("qty")
    private Double qty;

    @SerializedName("buyPrice")
    private Double buyPrice;

    public Integer getDetailId() {
        return detailId;
    }

    public void setDetailId(Integer detailId) {
        this.detailId = detailId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getOriginSource() {
        return originSource;
    }

    public void setOriginSource(String originSource) {
        this.originSource = originSource;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    @Override
    public String toString() {
        return "WalletDetail{" +
                "walletId='" + walletId + '\'' +
                ", symbol='" + symbol + '\'' +
                ", originSource='" + originSource + '\'' +
                ", qty=" + qty +
                ", buyPrice=" + buyPrice +
                '}';
    }
}
